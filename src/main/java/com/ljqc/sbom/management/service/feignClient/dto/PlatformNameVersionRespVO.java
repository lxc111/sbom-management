package com.ljqc.sbom.management.service.feignClient.dto;

import com.ljqc.sbom.management.dto.LicenseVO;
import com.ljqc.sbom.management.dto.VulPackageInfoVO;
import lombok.Data;

import java.util.List;

@Data
public class PlatformNameVersionRespVO {
    private Integer id;
    private Integer ljPackageId;
    private String packageName;
    private String platform;
    private String version;
    private Boolean isOpen;
    private String verifiedLicense;
    private Integer moveTo;
    private Integer licenseRelation;
    private List<VulPackageInfoVO> vulPackageInfoVOS;
    private List<LicenseVO> licenseVOS;
}
