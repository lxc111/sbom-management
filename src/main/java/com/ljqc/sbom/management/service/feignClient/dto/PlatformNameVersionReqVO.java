package com.ljqc.sbom.management.service.feignClient.dto;

import lombok.Data;

import java.util.List;

@Data
public class PlatformNameVersionReqVO {
    private List<PlatformNameVersionListVO> reqListVOS;
}
