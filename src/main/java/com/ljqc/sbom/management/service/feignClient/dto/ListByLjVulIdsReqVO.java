package com.ljqc.sbom.management.service.feignClient.dto;

import lombok.Data;

import java.util.List;

/**
 * <p>
 *
 * </p>
 *
 * @author wuhao
 * @date 2023/6/15
 */
@Data
public class ListByLjVulIdsReqVO {
    private List<String> vulIds;
}
