package com.ljqc.sbom.management.service.feignClient.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PlatformNameVersionListVO {

    private String platform;
    private String name;
    private String version;
    private Integer id;
}
