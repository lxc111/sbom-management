package com.ljqc.sbom.management.service;

import com.ljqc.sbom.management.constant.Platform;
import com.ljqc.sbom.management.domain.ConanPackage;
import com.ljqc.sbom.management.domain.GoPackage;
import com.ljqc.sbom.management.domain.MavenPackage;
import com.ljqc.sbom.management.domain.NpmPackage;
import com.ljqc.sbom.management.domain.PackageInfo;
import com.ljqc.sbom.management.domain.PypiPackage;
import com.ljqc.sbom.management.domain.RubyPackage;
import com.ljqc.sbom.management.repository.ConanPackageRepository;
import com.ljqc.sbom.management.repository.GoPackageRepository;
import com.ljqc.sbom.management.repository.MavenPackageRepository;
import com.ljqc.sbom.management.repository.NpmPackageRepository;
import com.ljqc.sbom.management.repository.PipPackageRepository;
import com.ljqc.sbom.management.repository.RubyPackageRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class RepositoryTransfer {

    @Autowired
    private MavenPackageRepository mavenPackageRepository;

    @Autowired
    private NpmPackageRepository npmPackageRepository;

    @Autowired
    PipPackageRepository pipPackageRepository;

    @Autowired
    RubyPackageRepository rubyPackageRepository;

    @Autowired
    ConanPackageRepository conanPackageRepository;

    @Autowired
    GoPackageRepository goPackageRepository;

    public PackageInfo findByLjPackageId(String platform, Integer ljPackageId) {
        PackageInfo packageInfo = new PackageInfo();
        if (platform.equalsIgnoreCase(Platform.maven.name())
                || Platform.gradle.name().equalsIgnoreCase(platform)
                || Platform.sbt.name().equalsIgnoreCase(platform)
                || Platform.ivy.name().equalsIgnoreCase(platform)
                || Platform.grape.name().equalsIgnoreCase(platform))
            platform = "maven2";
        switch (platform)
        {
            case "maven2":
                MavenPackage mavenPackage = mavenPackageRepository.findByLjPackageId(ljPackageId);
                if (mavenPackage == null) return null;
                BeanUtils.copyProperties(mavenPackage,packageInfo );
                return packageInfo;
            case "npm":
                NpmPackage npmPackage = npmPackageRepository.findByLjPackageId(ljPackageId);
                if (npmPackage == null) return null;
                BeanUtils.copyProperties(npmPackage,packageInfo );
                return packageInfo;
            case "pypi":
                PypiPackage pypiPackage = pipPackageRepository.findByLjPackageId(ljPackageId);
                if (pypiPackage == null) return null;
                BeanUtils.copyProperties(pypiPackage, packageInfo);
                return packageInfo;
            case "rubygems":
                RubyPackage rubyPackage = rubyPackageRepository.findByLjPackageId(ljPackageId);
                if (rubyPackage == null) return null;
                BeanUtils.copyProperties(rubyPackage, packageInfo);
                return packageInfo;
            case "conan":
                ConanPackage conanPackage = conanPackageRepository.findByLjPackageId(ljPackageId);
                if (conanPackage == null) return null;
                BeanUtils.copyProperties(conanPackage, packageInfo);
                return packageInfo;
            case "go":
                GoPackage goPackage = goPackageRepository.findByLjPackageId(ljPackageId);
                if (goPackage == null) return null;
                BeanUtils.copyProperties(goPackage, packageInfo);
                return packageInfo;
            default:
        }
        return packageInfo;
    }


    public Map<Integer, String> getMoveToSolution(String platform, Integer ljPackageId){
        Map<Integer, String> map = new HashMap<>(16);
        if (platform.equalsIgnoreCase(Platform.maven.name())
                || Platform.gradle.name().equalsIgnoreCase(platform)
                || Platform.sbt.name().equalsIgnoreCase(platform)
                || Platform.ivy.name().equalsIgnoreCase(platform)
                || Platform.grape.name().equalsIgnoreCase(platform))
            platform = "maven2";

        switch (platform) {
            case "maven2":
                MavenPackage mavenPackage = mavenPackageRepository.findByLjPackageId(ljPackageId);
                if (mavenPackage != null){
                    map.put(mavenPackage.getMoveTo(),"已更名为"+mavenPackage.getPackagePath()+","+"请替换为"
                            +mavenPackage.getPackagePath()+"并将版本升级到"+mavenPackage.getLatestRelease());
                    return map;
                }

            case "npm":
                NpmPackage npmPackage = npmPackageRepository.findByLjPackageId(ljPackageId);
                if (npmPackage != null){
                    map.put(npmPackage.getMoveTo(),"已更名为"+npmPackage.getPackageName()+","+"请替换为"
                            +npmPackage.getPackageName()+"并将版本升级到"+npmPackage.getLatestRelease());
                    return map;
                }
            case "pypi":
                PypiPackage pypiPackage = pipPackageRepository.findByLjPackageId(ljPackageId);
                if (pypiPackage != null){
                    map.put(pypiPackage.getMoveTo(),"已更名为"+pypiPackage.getPackageName()+","+"请替换为"
                            +pypiPackage.getPackageName()+"并将版本升级到"+pypiPackage.getLatestRelease());
                    return map;
                }
            case "rubygems":
                RubyPackage rubyPackage = rubyPackageRepository.findByLjPackageId(ljPackageId);
                if (rubyPackage != null){
                    map.put(rubyPackage.getMoveTo(),"已更名为"+rubyPackage.getPackageName()+","+"请替换为"
                            +rubyPackage.getPackageName()+"并将版本升级到"+rubyPackage.getLatestRelease());
                    return map;
                }
            case "conan":
                ConanPackage conanPackage = conanPackageRepository.findByLjPackageId(ljPackageId);
                if (conanPackage != null){
                    map.put(conanPackage.getMoveTo(),"已更名为"+conanPackage.getPackageName()+","+"请替换为"
                            +conanPackage.getPackageName()+"并将版本升级到"+conanPackage.getLatestRelease());
                    return map;
                }
            case "go":
                GoPackage goPackage = goPackageRepository.findByLjPackageId(ljPackageId);
                if (goPackage != null){
                    map.put(goPackage.getMoveTo(),"已更名为"+goPackage.getPackageName()+","+"请替换为"
                            +goPackage.getPackageName()+"并将版本升级到"+goPackage.getLatestRelease());
                    return map;
                }
            default:
        }
        return map;
    }


}
