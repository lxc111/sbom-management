package com.ljqc.sbom.management.repository;

import com.ljqc.sbom.management.domain.PypiPackage;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PipPackageRepository extends JpaRepository<PypiPackage, Integer>
{
    PypiPackage findByLjPackageId(Integer ljPackageId);

}
