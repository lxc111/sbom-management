package com.ljqc.sbom.management.repository;

import com.ljqc.sbom.management.domain.MavenPackage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Map;

public interface MavenPackageRepository extends JpaRepository<MavenPackage, Integer>
{

    MavenPackage findByLjPackageId(Integer packageId);

}
