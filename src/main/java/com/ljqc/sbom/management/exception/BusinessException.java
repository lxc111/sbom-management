package com.ljqc.sbom.management.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

/**
 * BusinessException
 */
@Getter
public class BusinessException extends RuntimeException {

    private HttpStatus status = HttpStatus.OK;

    private int code = HttpStatus.BAD_REQUEST.value();

    public BusinessException(String message) {
        super(message);
    }

    public BusinessException(String message, int code) {
        super(message);
        this.code = code;
    }

    public BusinessException(String message, HttpStatus status) {
        super(message);
        this.status = status;
    }

    public BusinessException(String message, int code, HttpStatus status) {
        super(message);
        this.code = code;
        this.status = status;
    }

    public BusinessException(Throwable cause) {
        super(cause);
    }

    public BusinessException(String message, Throwable cause) {
        super(message, cause);
    }
}
