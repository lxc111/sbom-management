package com.ljqc.sbom.management.bean;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public abstract class PageQuery {
    public static final int DEFAULT_PAGE_NO = 1;
    public static final int DEFAULT_PAGE_SIZE = 10;

    @NotNull
    @Min(value = 1)
    private int pageNo = DEFAULT_PAGE_NO;

    @NotNull
    @Min(value = 1)
    @Max(value = 500)
    private int pageSize = DEFAULT_PAGE_SIZE;
}
