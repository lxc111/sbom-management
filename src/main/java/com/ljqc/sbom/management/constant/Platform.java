package com.ljqc.sbom.management.constant;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public enum Platform
{
    maven, gradle, go, npm, composer, pypi, packagist, rubygems, nuget, cocoapods, wordpress, yarn, sbt,cargo, conan,ivy,swift,
    python,poetry,conda,jspm,bower,bundler,govendor,vendor,gomoudles,godep,hex,rust,linux,pear,cran,cpan,clojars,grape,pub,elm,shards,haxelib,nimble,purescript
    ;

    private static Logger logger = LoggerFactory.getLogger(Platform.class);

    public static boolean contains(String str)
    {
        for (Platform p : Platform.values())
        {
            if (p.name().equalsIgnoreCase(str))
            {
                return true;
            }
        }
        logger.info("can not find platform {} in enum", str);
        return false;
    }
}
