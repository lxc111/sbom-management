package com.ljqc.sbom.management.constant;

import com.google.common.collect.Lists;

import java.util.Arrays;
import java.util.List;

public class Constant
{
    // 加密分割符号
    public static final String separator = ":";

    // 一组公钥私钥对
    // 编码后的公钥
    public static final String BACKEND_PUBLIC_KEY = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC1z9ShzyLE6dkAcnbfRdxH7m+VVXDDpGOTTBt+Pzq4pjagargSlUkyOBoTY0nu6IXOlTAero6nGYQ/GL9ibT4oDUIMW9d94n+hYwOFCUZU7rNBpjJJ8UHqcXG92246aSW+tiEk9/FmnacskHGr3AapBVl4j/PE8b8DU3ydx75vwQIDAQAB";

    public static final String FRONTEND_PUBLIC_KEY = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDbUbIFnGRhS6L2JHnJDW6aeNJdS38L3ZrKUYZubxiNuyItaY/aam2EB1yQA7DahsRvyxQkVhdlX0mHYfV7bs/hluPbl28enGrdIEv0fhbiMd03kC8Wy2zke/UyAsYIcwlh09N+Ti21PrqfVcp/ZmOd4rB/v1vBAornun/b5BoHvwIDAQAB";

    // 编码后的私钥
    public static final String BACKEND_PRI_KEY = "MIICeAIBADANBgkqhkiG9w0BAQEFAASCAmIwggJeAgEAAoGBALXP1KHPIsTp2QBydt9F3Efub5VVcMOkY5NMG34/OrimNqBquBKVSTI4GhNjSe7ohc6VMB6ujqcZhD8Yv2JtPigNQgxb133if6FjA4UJRlTus0GmMknxQepxcb3bbjppJb62IST38WadpyyQcavcBqkFWXiP88TxvwNTfJ3Hvm/BAgMBAAECgYEAhJkH1d8Q15ZJancQ1TPJPTjbbok6zaT17naCUqfgtsyaaJKMnxj7okGiWuN+B1zUQd8ykh3y8lG8zpvI8QsFWceQR0Z9ytKP8PL7m8N+QDqyZUEB1N5xg7uz/NFyj+tIdM76ZCNjw4Ycj0dPciZ4iQOkLui5GH09dXyUlNyp2VUCQQD+tD5ft+jKwsWutUEBpNKyWhDrEz+C5dx3wTaX1BqsFWVg2KAAKdrN+eF7a1VuQ15pyVmFdxNoyzQSLJHpwaFvAkEAtrykwXbZ43Fy1aTMYySmyQZ9JeGzBnpsDVN7Pi9jUNl7VKJWsX+vc1xYoMvHNc4ifCulODdppodFo2QfooAJzwJAL6u4iN24+koeCabu9cnNjYgB5p2Bo5Q2AUvuVdhi7ZZLlSa2O5GZ3DdKlnjuUmbVguMmmC5DObfelRA2dxPAjQJBAJ/q3OA1657ApLdU/kzxQUiBdjoVl39e2XqcCSm1fNu/R9AcNXQmgtuZADLPwqlSmRlDdNaKn9nNXeMMdybVmD8CQQD8HcAqFKkXd4hwOF/XV+RqBk2/PmLxdvj1J2Wy/TYB6XGNEsy7vuCzgltHPO381/JS+JJ3fA7dosNBXXfGoio4";

    public static final String FRONTEND_PRI_KEY = "MIICeAIBADANBgkqhkiG9w0BAQEFAASCAmIwggJeAgEAAoGBANtRsgWcZGFLovYkeckNbpp40l1LfwvdmspRhm5vGI27Ii1pj9pqbYQHXJADsNqGxG/LFCRWF2VfSYdh9Xtuz+GW49uXbx6cat0gS/R+FuIx3TeQLxbLbOR79TICxghzCWHT035OLbU+up9Vyn9mY53isH+/W8ECiue6f9vkGge/AgMBAAECgYBtaeFNKUtuuqQu9d2H7tqJ19Li1kIockLBwlA1kdkHBB+11h79EPFX17BW7HBzZJ5lQ0PmKMj9Or9c+gLUtGCiNw1E4+hplo7uqaQCMaEVqVNdmJ77amrLEbC62nOwV4Uj2oihZACUeu41AULs8sU96qZGaTYYQG1rq6gT5H1SuQJBAO7z0HXjZhmRq8+e+1nu3nFs8dK7LYZldgSsRgve6NMDtYPhM+6IoBIaT0tdMjYQYDNTMzCEBzePIMiNtUlEmdMCQQDq901i+m6oycoWqIryCN9o5EoP8cz+GpBnrGufWBp4qkNuyOjszaVX05Nn6MEtFQLSAqzAuyqUQzl/HysuTxrlAkEA2ZUx0OcL5WR76kZ94dpcuzPUz2qZfbONz5t+/PgxUVQjSL8cJqxJ9bH3Wky2l/w5XQL5rcfLOpAd7gD7fof0+QJBANWyyDJOHFjs9hy3e293/GD0yBtKSquM02tHGLbatXCrQhs/JQFCY6/GINmvigoLgv4oKqA7WhVwMpnYajh6/7UCQQCnLFc42lz/ogKI8851C/0sc3oP+lgU5jUlIW2Uap8FBAIyOOcy9ss8ZAsvUgOVRr+grK3SOMLUVG/Mrrwo3Prn";

    public static final String CONFIG_PUBLIC_KEY = "MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAgxDY5xnm8q/tEzCc5cNcYFneM7AEr/6QKMqj64Kiet/5Mnd2g2U6sDhU0huKszkSnUyLrpFZqVoWTgTDFU6k6c0e1nSIPN7MLIGnayqLjM1T2X7uuuez+5EeUyzf8cdFmqKw2VZiR3ibRtD3QVaQtBO40ZdpE1IaBsj8ZAW2KUe9EL4roH+Rl9BZQ0XfolLlkfo72ARlHm7EFOs/5Wok0GzxGRPm8pOsoxVIvtSLYASpTdzifIDMbgtCsdNUohPIUuUyasEc/QebB66gtlYtp9OTy27ynaCcdSh2r+9u5qsiOfOYiAzyC4XphLbKmrnV0E0iDuju3FVW/Vs1Xs5Lm4NoP0dd6Ab60gV1Eux816POJD4RY7QwWc+KkBKk+XLhvaKqbBppQy9FiCLbpGE8KRivnMlQf2tea4taxzo89NA7ADmU8G4JargbW78qW5iW5ZRl3PmgTWFlZ28kUTB3e2UOKk6XVoXCKrX7ZW9G1NfqfETzdHZR56WBOU6zAdVXTYpSE1eZ/vJXItf66U4bjyiSD285eVUQi1NiS7Tah6HdcDBczFPBZwgyK2wF0ojirqW6VZ/NgE+kqIxzHwGFMxfAxwVjlRP0/9Jh7lLnHiO9Wqb8/OxH6NdD4YdWkumqLPc8RIX0JTbwBg2jONWPOWS+ewJBJZ306gTxyjkZjpsCAwEAAQ==";

    //平台列表
    public static final List<String> platformList = Arrays.asList("maven", "gradle", "go", "npm", "composer", "pypi", "packagist", "rubygems", "nuget", "cocoapods", "bower", "jar", "sbt", "yarn", "wordpress");

    //漏洞等级
    public static final List<String> vulLevelList = Arrays.asList("严重", "高危", "中危", "低危", "未评级", "未知");
//    许可证 高:3,中:2,低:1,未知:0
    public static final Integer HIGHT_LICENSE = 3;
    public static final Integer MID_LICENSE = 2;
    public static final Integer LOW_LICENSE = 1;
    public static final Integer UNKNOWN_LICENSE = 0;
//   漏洞  0:未评级 1:低危 2:中危 3:高危 4:严重超危
    public static final Integer SUPER_VUL = 4;
    public static final Integer HIGH_VUL = 3;
    public static final Integer MID_VUL = 2;
    public static final Integer LOW_VUL = 1;
    public static final Integer UNKNOWN_VUL = 0;

    //项目二进制包解析类型
    public static final Integer BINARY_TYPE0 = 0;//项目中包含的jar包
    public static final Integer BINARY_TYPE1 = 1;//单独的jar war包

    public static final String REPO_BLACK_LIST ="组件黑名单";
    public static final String LICENSE_BLACK_LIST ="许可证黑名单";

    public static final String VUL_BLACK_LIST ="漏洞黑名单";
    public static final String EXISTS_VULNERABILITY="存在漏洞";

    public static final String mailContentEnd = "金融科技监管合规：<br />\n" +
            " <ul><li>发现数据安全缺陷、漏洞等风险时，应当立即采取补救措施；按照规定及时告知用户并向有关主管部门报告;构成《中华人民共和国数据安全法》第二十九条规定情形的,造成大量数据泄露等严重后果的，处五十万元以上二百万元以下罚款，并责令暂停相关业务、停业整顿、吊销相关业务许可证或者吊销营业执照，对直接负责的主管人员和其他直接责任人员处五万元以上二十万元以下罚款.</li><li>第十二条 网络产品提供者未按本规定采取网络产品安全漏洞补救或者报告措施的，由工业和信息化部、公安部依据各自职责依法处理；构成《中华人民共和国网络安全法》第六十条规定情形的，依照网络安全法予以处罚.</li><li>第十三条 网络运营者未按本规定采取网络产品安全漏洞修补或者防范措施的，由有关主管部门依法处理；构成《中华人民共和国网络安全法》第五十九条规定情形的，依照网络安全法规定予以处罚.</li> </ul>\n";

    public static final String NEXUS = "nexus";
    public static final String NEXUS2 = "nexus2";
    public static final String CVSS_SCORE = "cvss_score";
    public static final String DESC = "desc";
    public static final Integer LJ_VUL_SCORE_MAX_VALUE = 100;
    public static final Integer LJ_VUL_SCORE_MIN_VALUE = 0;

    public static final Integer EXPOSURE_LEVEL_TYPE_LOW = 0;

    public static final String MAVEN_PLATFORM = "maven";

    // 组件引入方式类型集合
    public static final List<Integer> IMPORT_TYPE_LIST = Lists.newArrayList(-1, 0, 1);

    // 是与否类型集合
    public static final List<Integer> YES_OR_NO_LIST = Lists.newArrayList(0, 1);

    // 解决方案类型集合
    public static final List<Integer> SOLVE_TYPE_LIST = Lists.newArrayList(0, 1, 2);



}
