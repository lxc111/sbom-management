package com.ljqc.sbom.management.dto;

import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class SpdxPackageDTO {

    private Integer id;
    private Integer ljPackageId;
    private String spdxId;
    private String repoName;
    private String version;
    private String platform;
    private String license;
    private Boolean inhouse;
    private String homePage;
    private String summary;
    private String description;
    private String downloadLocation;
    private String supplier;
    private String originator;
    private String sourceInfo;
    private String licenseConcluded;
    private String licenseDeclared;
    private String copyrightText;
    private String verificationCode;
    private String checksum;
    private String relationship;
    private String comment;
    private String licenseComments;
    private String packageFileName;
    private Integer licenseRelation;

    private Map<String, Integer> vulMap;
    private List<String> licenses;
    private List<String> licenseDeclareds;
    private Integer licenseDeclaredRelation;


}
