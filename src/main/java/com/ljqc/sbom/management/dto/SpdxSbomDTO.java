package com.ljqc.sbom.management.dto;

import com.ljqc.sbom.management.domain.SpdxPackage;
import com.ljqc.sbom.management.domain.SpdxProject;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class SpdxSbomDTO {

    private SpdxProject project;

    private List<SpdxPackage> packages;
}
