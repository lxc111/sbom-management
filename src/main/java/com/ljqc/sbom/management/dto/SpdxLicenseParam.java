package com.ljqc.sbom.management.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class SpdxLicenseParam {
    @NotNull(message = "项目id不能为空")
    private Integer projectId;

    @NotNull(message = "当前页不能为空")
    private Integer pageNo;

    @NotNull(message = "分页条数不能为空")
    private Integer pageSize;

    private String license;

    private Integer riskLevel;



    /**
     * 排序字段
     */
    private String orderName;

    /**
     * 排序规则 DESC：降序 ASC：升序
     */
    private String orderType;



}
