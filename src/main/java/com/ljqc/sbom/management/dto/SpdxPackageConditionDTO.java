package com.ljqc.sbom.management.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class SpdxPackageConditionDTO {
    @ApiModelProperty("包管理器")
    private List<String> platformList;
}
