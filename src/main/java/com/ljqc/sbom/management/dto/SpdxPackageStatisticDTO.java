package com.ljqc.sbom.management.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Map;

@Data
public class SpdxPackageStatisticDTO {
    @ApiModelProperty("安全风险统计")
    private Map<String, Object> securityMap;

    @ApiModelProperty("许可证风险统计")
    private Map<String, Object> licenseMap;
}
