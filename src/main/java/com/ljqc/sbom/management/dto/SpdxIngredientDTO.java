package com.ljqc.sbom.management.dto;


import lombok.Data;

@Data
public class SpdxIngredientDTO {
    private int pageNo;
    private int pageSize;
    private Integer projectId;
    private Integer vulLevel;
    private String platform;
    private String name;
    private Integer ljVulScoreMax;
    private Integer ljVulScoreMin;
}
