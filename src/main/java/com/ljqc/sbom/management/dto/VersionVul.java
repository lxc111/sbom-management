package com.ljqc.sbom.management.dto;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class VersionVul
{
    @Id
    @GeneratedValue
    private String id;
    private String version;
    private String publishedTime;
    private String license;
    private String cve;
    private String ljId;

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getVersion()
    {
        return version;
    }

    public void setVersion(String version)
    {
        this.version = version;
    }

    public String getCve()
    {
        return cve;
    }

    public void setCve(String cve)
    {
        this.cve = cve;
    }

    public String getLjId()
    {
        return ljId;
    }

    public void setLjId(String ljId)
    {
        this.ljId = ljId;
    }

    public String getPublishedTime()
    {
        return publishedTime;
    }

    public void setPublishedTime(String publishedTime)
    {
        this.publishedTime = publishedTime;
    }

    public String getLicense()
    {
        return license;
    }

    public void setLicense(String license)
    {
        this.license = license;
    }
}
