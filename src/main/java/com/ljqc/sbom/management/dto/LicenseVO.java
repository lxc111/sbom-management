package com.ljqc.sbom.management.dto;

import lombok.Data;

@Data
public class LicenseVO {
    private String title;
    private String ljLicenseKey;
    private String licenseRiskLevel;
    private String categoryEn;
    private String categoryCh;
}
