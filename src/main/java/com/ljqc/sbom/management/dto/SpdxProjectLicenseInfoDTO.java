package com.ljqc.sbom.management.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

/**
 *  spdx项目概览页面许可证风险
 */
@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class SpdxProjectLicenseInfoDTO {
    @ApiModelProperty("许可证风险统计")
    private List<LicenseRiskInfo> licenseRiskInfos;
    @ApiModelProperty("许可证总数")
    private Integer licenseCount;
    @ApiModelProperty("高风险许可证总数")
    private Integer highLicenseCount;
    /**
     *  许可证风险等级
     */
    @Data
    @Builder
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class LicenseRiskInfo{
        @ApiModelProperty("风险等级")
        private Integer riskLevel;
        @ApiModelProperty("风险等级对应数量")
        private Integer riskLevelCount;
    }

}
