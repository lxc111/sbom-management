package com.ljqc.sbom.management.dto;

import lombok.Data;

import java.util.List;

@Data
public class SpdxVulListDTO {
    private Integer pageNo ;
    private Integer pageSize ;
    private Integer projectId ;
    private List<Integer> vulLevels ;
    private String vulId ;
    private String field ;
    private String order ;
    private Integer ljVulScoreMax ;
    private Integer ljVulScoreMin ;
}
