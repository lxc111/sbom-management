package com.ljqc.sbom.management.dto;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.ArrayList;
import java.util.List;

public class LicenseInfoVo
{
    private List<String> names = new ArrayList<>();
    @JSONField(name = "risk_levels")
    private List<Integer> riskLevels = new ArrayList<>();
    private List<String> connector = new ArrayList<>();

    public List<String> getNames()
    {
        return names;
    }

    public void setNames(List<String> names)
    {
        this.names = names;
    }

    public void addNames(String name)
    {
        this.names.add(name);
    }

    public List<Integer> getRiskLevels()
    {
        return riskLevels;
    }

    public void setRiskLevels(List<Integer> riskLevels)
    {
        this.riskLevels = riskLevels;
    }

    public void addRiskLevels(Integer riskLevel)
    {
        this.riskLevels.add(riskLevel);
    }

    public List<String> getConnector()
    {
        return connector;
    }

    public void setConnector(List<String> connector)
    {
        this.connector = connector;
    }

    public void addConnector(String connector)
    {
        this.connector.add(connector);
    }
}
