package com.ljqc.sbom.management.dto;

import lombok.Data;

import java.util.List;

@Data
public class SpdxProjectInfo {

    private String SPDXID;
    private String spdxVersion;
    private String dataLicense;
    // 文档名称
    private String name;
    private String documentNamespace;
    private CreationInfo creationInfo;
    private String comment;
    private List<ExternalDocumentRef> externalDocumentRefs;
    private List<Package> packages;
    private List<Relationship> relationships;

    @Data
    public static class CreationInfo {
        private String licenseListVersion;
        private String created;
        private String comment;
        private List<String> creators;
    }

    @Data
    public static class ExternalDocumentRef {
        private String spdxDocument;
    }

    @Data
    public static class Package {
        private String name;
        private String SPDXID;
        private String versionInfo;
        private String packageFileName;
        private String supplier;
        private String originator;
        private String downloadLocation;
        private PackageVerificationCode packageVerificationCode;
        private List<Checksum> checksums;
        private String homepage;
        private String sourceInfo;
        private String licenseConcluded;
        private String licenseDeclared;
        private String licenseComments;
        private String copyrightText;
        private String summary;
        private String description;
        private String comment;
        private List<ExternalRef> externalRefs;
    }

    @Data
    public static class PackageVerificationCode {
        private List<String> packageVerificationCodeExcludedFiles;
        private String packageVerificationCodeValue;
    }

    @Data
    public static class Checksum {
        private String algorithm;
        private String checksumValue;
    }

    @Data
    public static class ExternalRef {
        // 包管理器
        private String referenceType;
        private String referenceCategory;
        private String referenceLocator;
    }

    @Data
    public static class Relationship {
        private String relationshipType;
        private String relatedSpdxElement;
        private String spdxElementId;
    }


}
