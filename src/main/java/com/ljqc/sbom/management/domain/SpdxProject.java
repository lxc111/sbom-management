package com.ljqc.sbom.management.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "spdx_project")
@Getter
@Setter
public class SpdxProject {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private String documentName;
    private Integer type;
    private String projectVersion;
    private String projectLicense;
    private String licenseListVersion;
    private String spdxId;
    private String creatorPerson;
    private String creatorOrganization;
    private String creatorTool;
    private String documentNamespace;
    private String documentComment;
    private String creatorComment;
    private String created;
    private String result;
    private Integer netType;
    private Integer status = 0;
    private String msg;
    private Integer progress;
    private String fileName;
    private String filePath;
    private Integer repoVulLevel;
    private Integer licenseRiskLevel;
    private Long createTime;
    private Long updateTime;
    private Long lastScan;
    private Integer userId;
    private Integer enterpriseId;

}
