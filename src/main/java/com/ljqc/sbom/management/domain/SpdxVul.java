package com.ljqc.sbom.management.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Table(name = "spdx_vul")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SpdxVul {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private Integer spdxProjectId;
    private Integer spdxPackageId;
    private String vulId;
    private String cveId;
    private String cwes;
    private String vulTitle;
    private Integer vulLevel;
    private String cvssScore;
    private BigDecimal ljVulScore;
    private Integer exposureLevel;
    private LocalDateTime vulPublished;
    private String solve;


}
