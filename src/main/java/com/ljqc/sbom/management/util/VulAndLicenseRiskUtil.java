package com.ljqc.sbom.management.util;

/**
 * @author sunzhuo
 * @create 2022-05-09
 */
public class VulAndLicenseRiskUtil {


    public static int vul_cn(String vul) {
        if (vul == null) {
            return 0;
        }
        int num;
//        (0:未评级 1:低危 2:中危 3:高危 4:超危)
        switch (vul) {
            case "40":
                num = 4;
                break;
            case "30":
                num = 3;
                break;
            case "20":
                num = 2;
                break;
            case "10":
                num = 1;
                break;
            default:
                num = 0;
        }
        return num;
    }

    public static String vulDescCn(String vul) {
        if (vul == null) {
            return "未评级";
        }
        String desc;
//        (0:未评级 1:低危 2:中危 3:高危 4:超危)
        switch (vul) {
            case "40":
                desc = "超危";
                break;
            case "30":
                desc = "高危";
                break;
            case "20":
                desc = "中危";
                break;
            case "10":
                desc = "低危";
                break;
            default:
                desc = "未评级";
        }
        return desc;
    }

    public static String vulDescEn(String vul) {
        if (vul == null) {
            return "未评级";
        }
        String desc;
//        (0:未评级 1:低危 2:中危 3:高危 4:超危)
        switch (vul) {
            case "40":
                desc = "CRITICAL";
                break;
            case "30":
                desc = "HIGH";
                break;
            case "20":
                desc = "MEDIUM";
                break;
            case "10":
                desc = "LOW";
                break;
            default:
                desc = "Not-standard";
        }
        return desc;
    }
    /**
     *  low(0, "LOW", "低"),
     *  medium(1, "MEDIUM", "中"),
     * high(2, "HIGH", "高");
    */
    public static String useLevelEn(String vul) {
        if (vul == null) {
            return "LOW";
        }
        String desc;
        switch (vul) {
            case "0":
                desc = "LOW";
                break;
            case "1":
                desc = "MEDIUM";
                break;
            case "2":
                desc = "HIGH";
                break;
            default:
                desc = "LOW";
        }
        return desc;
    }

    public static String useLevelCn(String vul) {
        if (vul == null) {
            return "低";
        }
        String desc;
        switch (vul) {
            case "0":
                desc = "低";
                break;
            case "1":
                desc = "中";
                break;
            case "2":
                desc = "高";
                break;
            default:
                desc = "低";
        }
        return desc;
    }

}
