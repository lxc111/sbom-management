package com.ljqc.sbom.management.util;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ResultCode {
    SUCCESS(200, "成功"),
    UNKNOWN_ERROR(500, "unknown error"),
    SERVER_EXEC_ERROR(501, "server exec error"),

    CLIENT_ERROR(400, "Client端错误"),
    CLIENT_PARAM_ERROR(410, "Client请求参数错误"),
    RESOURCE_CHANGE(411,"所请求的资源已发生变动导致业务无法进行"),
    PERMISSION_DENIED(420, "权限不足"),
    TOO_FAST(430, "请求频率过高"),
    TIMES_LIMIT(440, "次数超过限制"),
    NOT_FOUND(450, "资源不存在(或已删除)"),
    NOT_LOGIN(460, "您还未登录"),
    NOT_CERTIFICATE(470, "企业未认证"),


    OPERATE_FAIL(3001, "操作失败"),

    LICENSE_BACK_LIST_AND_WHITELIST_CONFLICTS(4000, "许可证黑白名单冲突"),
    DEFECT_BACK_LIST_AND_WHITELIST_CONFLICTS(4001, "漏洞黑白名单冲突"),
    REPO_BACK_LIST_AND_WHITELIST_CONFLICTS(4002, "组件黑白名单冲突");

    // 错误码 只有200表示成功
    private final int code;
    private final String message;
}
