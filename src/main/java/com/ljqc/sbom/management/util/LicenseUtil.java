package com.ljqc.sbom.management.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LicenseUtil {
    public static int getCategoryLevel(String category) {
        switch (category) {
            case "私有":
                return 1;
            case "版权限制":
                return 2;
            case "版权":
                return 3;
            case "商业":
                return 4;
            case "专有自由":
                return 5;
            case "自由限制":
                return 6;
            case "公共领域":
                return 7;
            case "宽松":
                return 8;
            case "开源":
                return 9;
            case "未声明":
                return 10;
        }
        return Integer.MAX_VALUE;
    }

    public static int getLicenseRiskLevel(String licenseRiskLevel) {
        if (licenseRiskLevel == null) licenseRiskLevel = "";
        int riskLevel;
        switch (licenseRiskLevel) {
            case "高":
                riskLevel = 3;
                break;
            case "中":
                riskLevel = 2;
                break;
            case "低":
                riskLevel = 1;
                break;
            default:
                riskLevel = 0;
                break;
        }
        return riskLevel;
    }

    /**
     * tl 表缓存
     */
//    public static Map<String, LicenseBaseInfoTl> getLicenseBaseInfoTlMap(LicenseBaseInfoTlRepository licenseBaseInfoTlRepository, RedisService redisService)
//    {
//        Object licenseBaseInfoTlCacheObj = redisService.getObj("licenseBaseInfoTl");
//        if (licenseBaseInfoTlCacheObj != null)
//            return (Map<String, LicenseBaseInfoTl>) licenseBaseInfoTlCacheObj;
//
//        Map<String, LicenseBaseInfoTl> licenseBaseInfoTlCache = new HashMap<>();
//        List<LicenseBaseInfoTl> licenseBaseInfoTlList = licenseBaseInfoTlRepository.findAll();
//        for (LicenseBaseInfoTl licenseBaseInfoTl : licenseBaseInfoTlList)
//        {
//            if (!org.apache.commons.lang.StringUtils.isBlank(licenseBaseInfoTl.getSlug()))
//                licenseBaseInfoTlCache.put(licenseBaseInfoTl.getSlug().trim().toLowerCase(), licenseBaseInfoTl); //转小写
//            if (!org.apache.commons.lang.StringUtils.isBlank(licenseBaseInfoTl.getLicenseKey()))
//                licenseBaseInfoTlCache.put(licenseBaseInfoTl.getLicenseKey().trim().toLowerCase(), licenseBaseInfoTl);//转小写
//            if (!org.apache.commons.lang.StringUtils.isBlank(licenseBaseInfoTl.getShortName()))
//                licenseBaseInfoTlCache.put(licenseBaseInfoTl.getShortName().trim().toLowerCase(), licenseBaseInfoTl);//转小写
//            if (!StringUtils.isBlank(licenseBaseInfoTl.getSpdxId()))
//                licenseBaseInfoTlCache.put(licenseBaseInfoTl.getSpdxId().trim().toLowerCase(), licenseBaseInfoTl);//转小写
//        }
//        redisService.setObj("licenseBaseInfoTl", licenseBaseInfoTlCache);
//        return licenseBaseInfoTlCache;
//    }


}
