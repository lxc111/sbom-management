package com.ljqc.sbom.management.util;

import com.alibaba.fastjson.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * &#064;description:
 * &#064;author SongZelin
 * &#064;date 2022/7/20 21:09
 * &#064;Version
 */
public final class ThreadLocalManager {
    public static final String USER_JSON = "USER";
    private static final Integer USER_ROLE_ADMIN = 1;

    private ThreadLocalManager() {
    }

    private static final ThreadLocal<Map<String, Object>> CONTAINER = ThreadLocal.withInitial(HashMap::new);

    static {
        ThreadLocalCleaner.register(CONTAINER);
    }

    /**
     * @return threadLocal中的全部值
     */
    public static Map<String, Object> getAll() {
        return new HashMap<>(CONTAINER.get());
    }


    /**
     * 设置一个值到ThreadLocal
     * 注意：最好加上业务前缀保证key不会重复！
     *
     * @param key   键
     * @param value 值
     * @param <T>   值的类型
     * @return 被放入的值
     * @see Map#put(Object, Object)
     */
    public static <T> T put(String key, T value) {
        CONTAINER.get().put(key, value);
        return value;
    }

    /**
     * 设置所有制到ThreadLocal
     * 注意：最好加上业务前缀保证key不会重复！
     *
     * @param kv 键值
     */
    public static void putAll(Map<String, Object> kv) {
        CONTAINER.get().putAll(kv);
    }

    /**
     * 删除参数对应的值
     *
     * @param key
     * @see Map#remove(Object)
     */
    public static void remove(String key) {
        CONTAINER.get().remove(key);
    }

    /**
     * 清空ThreadLocal
     *
     * @see Map#clear()
     */
    public static void clear() {
        CONTAINER.remove();
    }

    /**
     * 从ThreadLocal中获取值
     *
     * @param key 键
     * @param <T> 值泛型
     * @return 值, 不存在则返回null, 如果类型与泛型不一致, 可能抛出{@link ClassCastException}
     * @see Map#get(Object)
     * @see ClassCastException
     */
    public static <T> T get(String key) {
        return ((T) CONTAINER.get().get(key));
    }


    /**
     * 获取一个值后然后删除掉
     *
     * @param key 键
     * @param <T> 值类型
     * @return 值, 不存在则返回null
     * @see this#get(String)
     * @see this#remove(String)
     */
    public static <T> T getAndRemove(String key) {
        try {
            return get(key);
        } finally {
            remove(key);
        }
    }

    public static Integer getCurrentUserId() {
        JSONObject json = JSONObject.parseObject(get(USER_JSON));
        return json.getInteger("id");
    }

    public static JSONObject getUserJson(){
        return JSONObject.parseObject(get(USER_JSON));
    }

    public static Integer getCurrentUserEnterpriseId() {
        JSONObject json = JSONObject.parseObject(get(USER_JSON));
        return json.getInteger("enterprise_id");
    }

    public static Boolean currentUserAdmin() {
        JSONObject json = JSONObject.parseObject(get(USER_JSON));
        return USER_ROLE_ADMIN.equals(json.getInteger("role"));
    }

    public static Boolean currentUserAdmin(JSONObject json) {
        return USER_ROLE_ADMIN.equals(json.getInteger("role"));
    }

    public static Integer getCurrentUserId(JSONObject json) {
        return json.getInteger("id");
    }

    public static Integer getCurrentUserEnterpriseId(JSONObject json) {
        return json.getInteger("enterprise_id");
    }

}
