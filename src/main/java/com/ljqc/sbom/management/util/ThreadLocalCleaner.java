package com.ljqc.sbom.management.util;

import org.apache.commons.lang3.ArrayUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * &#064;description:
 * &#064;author SongZelin
 * &#064;date 2022/7/20 21:13
 * &#064;Version
 */
public class ThreadLocalCleaner {

    private static final List<ThreadLocal<?>> CONTAINER = new ArrayList<>();

    public static void register(ThreadLocal<?>... threadLocal) {
        if(ArrayUtils.isNotEmpty(threadLocal)) {
            CONTAINER.addAll(Arrays.asList(threadLocal));
        }
    }


    public static void clear() {
        CONTAINER.forEach(ThreadLocal::remove);
    }

}


