package com.ljqc.sbom.management.util;


import org.spdx.tools.SpdxToolsHelper;

public class SpdxUtil {

    public static SpdxToolsHelper.SerFileType formatIntegerToSpdx(Integer type) {
        String spdxType;
        switch (type) {
            case 0:
                spdxType = "tag";
                break;
            case 1:
                spdxType = "json";
                break;
            case 2:
                spdxType = "xml";
                break;
            case 3:
                spdxType = "yaml";
                break;
            default:
                return null;
        }
        return SpdxToolsHelper.strToFileType(spdxType);
    }

}
