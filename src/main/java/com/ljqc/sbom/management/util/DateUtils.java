package com.ljqc.sbom.management.util;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Optional;

/**
 * DateUtils
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class DateUtils {
    private static final ZoneId SYSTEM_ZONE_ID = ZoneId.systemDefault();

    public static Long toTimestamp(LocalDateTime time) {
        return Optional.ofNullable(time).map(l -> l.atZone(SYSTEM_ZONE_ID).toInstant().toEpochMilli()).orElse(null);
    }

    public static LocalDateTime toLocalDateTime(Long timestamp) {
        return Instant.ofEpochMilli(timestamp).atZone(SYSTEM_ZONE_ID).toLocalDateTime();
    }
}
